import Button from '@material-ui/core/Button';
import React from 'react';
import { useState, useReducer } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Paper from '@material-ui/core/Paper';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import ReactFlow from 'react-flow-renderer';
import Image from './resources/tree.svg';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
  page: {
    backgroundImage: `url(${Image})`,
    height: '100vh',
    backgroundSize: "cover",
    opacity: '1',
      }
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function reducer(state, action) {
  switch (action.type) {
    case 'toggle':
      return {dark: !state.dark}
    default:
      throw new Error();
  }
}

export default function App() {

  const styles = useStyles();

  const elements = [
    { id: '1', data: { label: 'Node 1' }, position: { x: 250, y: 5 } },
    // you can also pass a React component as a label
    { id: '2', data: { label: <div>Node 2</div> }, position: { x: 100, y: 100 } },
    { id: 'e1-2', source: '1', target: '2', animated: true },
  ];
  
  return (
    <Paper className={styles.page}>
      test 1
      {/* <ReactFlow elements={elements} /> */}
      test 2
    </Paper>
  );
}