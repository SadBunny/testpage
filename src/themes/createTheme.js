import {createMuiTheme} from "@material-ui/core";

const PRIMARY_COLOR = '#333340';
const PRIMARY_CONTRAST_TEXT_COLOR = '#aaaabb';
const SECONDARY_COLOR = '#aaaa66';
const SECONDARY_COLOR_CONTRAST_TEXT = '#442244';
const ERROR_COLOR = '#e00000';
const ERROR_COLOR_CONTRAST_TEXT = '#ffffff';
const TEXT_PRIMARY_COLOR = '#333340';
const TEXT_SECONDARY_COLOR = '#666640';
const BACKGROUND_COLOR = '#d2ffd9';
const PAPER_BACKGROUND_COLOR = '#ccddcc';

const palette = {
    primary: {
        main: PRIMARY_COLOR,
        contrastText: PRIMARY_CONTRAST_TEXT_COLOR
    },
    secondary: {
        main: SECONDARY_COLOR,
        contrastText: SECONDARY_COLOR_CONTRAST_TEXT
    },
    error: {
        main: ERROR_COLOR,
        contrastText: ERROR_COLOR_CONTRAST_TEXT
    },
    text: {
        primary: TEXT_PRIMARY_COLOR,
        secondary: TEXT_SECONDARY_COLOR,
        hint: TEXT_PRIMARY_COLOR
    },
    background: {
        paper: PAPER_BACKGROUND_COLOR,
        default: BACKGROUND_COLOR
    }
};

const theme = {
    borderRadius: 2,
    palette: palette,
    typography: {
        fontFamily: 'Roboto',
        htmlFontSize: 16,
        fontSize: 16
    },
    props: {
        MuiButtonBase: {
            disableRipple: false, // Ripple, me likey!
        }
    }
};

export function createTheme() {
    return createMuiTheme(theme);
}

export default createTheme;

